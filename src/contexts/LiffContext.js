import React, { useContext } from 'react';
import { Skeleton, Result } from 'antd';
import { useAsync } from 'react-use';
import liff from '@line/liff';

const LiffContext = React.createContext()
const LiffContextProvider = ({
    children 
}) => {
    const {
        loading,
        value: profile,
        error
    } = useAsync(async () => {
        await liff.init({
            liffId: '1654864948-x134n8Ag'
        })
        if (!liff.isLoggedIn()) {
            // redirect
            return Promise.reject(new Error('Not loggedin'))
        }
        return liff.getProfile()
    },[])
    if (loading) {
        return <Skeleton />
    }
    if (error) {
        if (error.message === 'Not loggedin') {
            // redirect login
            liff.login({
                redirectUri: `${window.location.origin}`
            })
        }
        return <Result status="error" title={error.message} />
    }
    const value = {
        profile
    }
    return (
        <LiffContext.Provider value={value}>
            {children}
        </LiffContext.Provider>
    )
}

const useLiff = () => {
    const context = useContext(LiffContext)
    if (!context) {
        throw new Error('Cannot use useLiff outside LiffContextProvider')
    }
    return context
}

export default LiffContextProvider
export {
    useLiff
}

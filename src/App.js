import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import LiffContextProvider from "./contexts/LiffContext";

function App() {
  return (
  <BrowserRouter>
  <LiffContextProvider>
    <Route path="/">
      <Home />
    </Route>
    </LiffContextProvider>
  </BrowserRouter>
  );
}

export default App;

import React from 'react';
import { useLiff } from '../contexts/LiffContext';


const Home = () => {
    const {
        profile
    } = useLiff()

    return (
        <div>
            {profile.displayName}
        </div>
    )
}

export default Home
